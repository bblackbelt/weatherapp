package de.weatherapp;

import android.app.Application;

/**
 * Created by emanuele on 04.07.16.
 */
public class WeatherApplication extends Application {

    private static WeatherApplication sWeatherApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        sWeatherApplication = this;
    }

    public static synchronized WeatherApplication getInstance() {
        return sWeatherApplication;
    }
}
