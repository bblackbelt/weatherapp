package de.weatherapp.core;

import rx.Scheduler;

/**
 * Created by emanuele on 03.07.16.
 */
public interface RxSchedulerWrapper {
    Scheduler getSubscribeOn();
    Scheduler getObserveOn();
}
