package de.weatherapp.core;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by emanuele on 03.07.16.
 */
public class RxSchedulerWrapperImpl implements RxSchedulerWrapper {
    @Override
    public Scheduler getSubscribeOn() {
        return Schedulers.io();
    }

    @Override
    public Scheduler getObserveOn() {
        return AndroidSchedulers.mainThread();
    }
}
