package de.weatherapp.data.interactor;

import de.weatherapp.core.RxSchedulerWrapper;
import de.weatherapp.data.entity.WeatherRequest;
import de.weatherapp.data.entity.Weather;
import de.weatherapp.data.net.RestClient;
import rx.Observable;

/**
 * Created by emanuele on 03.07.16.
 */
public class WeatherInteractor extends Interactor<Weather, WeatherRequest> {

    private WeatherRequest mRequest;

    public WeatherInteractor(RxSchedulerWrapper schedulerWrapper) {
        super(schedulerWrapper);
    }

    public WeatherInteractor(WeatherRequest request, RxSchedulerWrapper schedulerWrapper) {
        super(schedulerWrapper);
        mRequest = request;
    }


    public void setRequest(WeatherRequest request) {
        mRequest = request;
    }

    @Override
    protected Observable<Weather> buildInteractorObservable() {
        return RestClient.getInstance().getWeather(mRequest);
    }
}
