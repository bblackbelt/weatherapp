package de.weatherapp.presentation.adapter;

import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.weatherapp.R;
import de.weatherapp.data.entity.WeatherForecast;
import de.weatherapp.presentation.Utils;
import de.weatherapp.presentation.model.Forecast;

/**
 * Created by emanuele on 04.07.16.
 */
public class FiveDaysForecastAdapter extends RecyclerView.Adapter<FiveDaysForecastAdapter.FiveDaysForecastViewHolder> {

    public static class FiveDaysForecastViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView mTemp;
        private final TextView mDay;
        private final ImageView mWeatherIcon;

        private Forecast mData;

        public FiveDaysForecastViewHolder(final View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mTemp = (TextView) itemView.findViewById(R.id.temp);
            mDay = (TextView) itemView.findViewById(R.id.date);
            mWeatherIcon = (ImageView) itemView.findViewById(R.id.weather_icon);
        }

        public void setWeather(Forecast data) {
            mData = data;
            final String tempString = Utils.kelvinToCelsius(data.getMinTemp()).concat(" / ").concat(Utils.kelvinToCelsius(data.getMaxTemp()));
            Spannable span = new SpannableString(tempString);
            final String minTempString = Utils.kelvinToCelsius(data.getMinTemp());
            int start = tempString.indexOf(minTempString);
            span.setSpan(new RelativeSizeSpan(0.7f), start, minTempString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mTemp.setText(span);
            mDay.setText(Utils.getDay(mDay.getContext(), data.getTimeStamp()));
            if (!TextUtils.isEmpty(mData.getIcon())) {
                Picasso.with(itemView.getContext()).load("http://openweathermap.org/img/w/"
                        .concat(mData.getIcon())).into(mWeatherIcon);
            }
        }


        @Override
        public void onClick(View v) {
            String title = mData.getCityName();
            if (title != null) {
                title = title.concat(" ").concat(Utils.getBigFormatDate(mData.getTimeStamp()));
            } else {
                title = Utils.getBigFormatDate(mData.getTimeStamp());
            }
            AlertDialog alertDialog = new AlertDialog.Builder(v.getContext())
                    .setTitle(title)
                    .setAdapter(new ArrayAdapter<WeatherForecast>(v.getContext(),
                            android.R.layout.simple_list_item_1, mData.getHourlyForecast()) {
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            if (convertView == null) {
                                convertView = LayoutInflater.from(parent.getContext())
                                        .inflate(R.layout.hourly_forecast_row, parent, false);
                            }
                            TextView temp = (TextView) convertView.findViewById(R.id.temp);
                            String maxTemp = null;
                            if (getItem(position).getMainWeatherInfo() != null) {
                                maxTemp = Utils.kelvinToCelsius(getItem(position).getMainWeatherInfo().getTempMax());
                            }
                            temp.setText(maxTemp);
                            TextView description = (TextView) convertView.findViewById(R.id.weather_description);
                            description.setText(getItem(position).getDescription());

                            TextView date = (TextView) convertView.findViewById(R.id.weather_date);
                            date.setText(Utils.getTime(getItem(position).getDt() * 1000));

                            ImageView weatherIcon = (ImageView) convertView.findViewById(R.id.weather_icon);
                            final String iconUrl = "http://openweathermap.org/img/w/"
                                    .concat(getItem(position).getIcon());
                            Picasso.with(weatherIcon.getContext()).load(iconUrl).into(weatherIcon);
                            return convertView;
                        }
                    }, null)
                    .create();
            alertDialog.show();
        }
    }

    private List<? extends Forecast> mDataSet;

    @Override
    public FiveDaysForecastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FiveDaysForecastViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_row, parent, false));
    }

    @Override
    public void onBindViewHolder(FiveDaysForecastViewHolder holder, int position) {
        holder.setWeather(mDataSet.get(position));
    }

    public void setData(List<? extends Forecast> dataSet) {
        mDataSet = dataSet;
    }

    @Override
    public int getItemCount() {
        return mDataSet == null ? 0 : mDataSet.size();
    }
}
