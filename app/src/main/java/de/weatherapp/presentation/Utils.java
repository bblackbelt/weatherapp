package de.weatherapp.presentation;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import de.weatherapp.R;

/**
 * Created by emanuele on 04.07.16.
 */
public class Utils {


    private static final SimpleDateFormat sTimeFormat = new SimpleDateFormat("HH:mm");
    private static final SimpleDateFormat sDateFormat = new SimpleDateFormat("MMMM d, yyyy");
    private static final SimpleDateFormat sBigDateFormat = new SimpleDateFormat("EEEE, MMM d");
    private static final SimpleDateFormat sDayFormat = new SimpleDateFormat("EEEE");
    private static final double ONE_KELVIN = 273.15;


    public static String getFormattedDate(long ms) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(ms);
        return sDateFormat.format(calendar.getTime());
    }

    public static String kelvinToCelsius(final double temp) {
        final int temperature = (int) Math.round(temp - ONE_KELVIN);
        return String.valueOf(temperature).concat("\u00B0");
    }

    public static String getDay(Context context, long ms) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(ms);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Calendar compareCalendar = Calendar.getInstance();
        compareCalendar.set(Calendar.HOUR, 0);
        compareCalendar.set(Calendar.MINUTE, 0);
        compareCalendar.set(Calendar.SECOND, 0);
        compareCalendar.set(Calendar.MILLISECOND, 0);

        if (isSameDay(calendar, compareCalendar)) {
            return context.getString(R.string.today);
        } else {
            compareCalendar.add(Calendar.DAY_OF_MONTH, 1);
            if (isSameDay(calendar, compareCalendar)) {
                return context.getString(R.string.tomorrow);
            }
        }
        calendar.setTimeInMillis(ms);
        return sDayFormat.format(calendar.getTime());
    }

    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }

    public static String getToday() {
        return getFormattedDate(System.currentTimeMillis());
    }

    public static String getBigFormatDate(long timeStamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp);
        return sBigDateFormat.format(timeStamp);
    }

    public static String getTime(long timeStamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp);
        return sTimeFormat.format(calendar.getTime());
    }
}
