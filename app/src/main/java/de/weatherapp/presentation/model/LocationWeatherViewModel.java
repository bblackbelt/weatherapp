package de.weatherapp.presentation.model;

import android.graphics.Bitmap;

import java.util.LinkedHashMap;
import java.util.Map;

import de.weatherapp.data.entity.City;
import de.weatherapp.data.entity.WeatherForecast;

/**
 * Created by emanuele on 03.07.16.
 *
 * This class represents 5 days forecast for a location
 *
 */
public class LocationWeatherViewModel implements ViewModel {

    private Map<String, WeatherForecastViewModel> mDayWeatherInfo = new LinkedHashMap<>();
    private City mCity;

    public void addWeatherInfo(String date, WeatherForecast weatherForecast) {
        WeatherForecastViewModel weatherForecastViewModel;
        if ((weatherForecastViewModel = mDayWeatherInfo.get(date)) == null) {
            weatherForecastViewModel = new WeatherForecastViewModel();
            mDayWeatherInfo.put(date, weatherForecastViewModel);
        }
        weatherForecastViewModel.setCity(mCity);
        weatherForecastViewModel.add(weatherForecast);
    }

    public Map<String, WeatherForecastViewModel> getDayWeatherInfo() {
        return mDayWeatherInfo;
    }

    public City getCity() {
        return mCity;
    }

    public void setCity(City mCity) {
        this.mCity = mCity;
    }

    public String getIconId() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocationWeatherViewModel that = (LocationWeatherViewModel) o;

        return mCity != null ? mCity.equals(that.mCity) : that.mCity == null;

    }

    @Override
    public int hashCode() {
        return mCity != null ? mCity.hashCode() : 0;
    }
}
