package de.weatherapp.presentation.model;

import java.util.List;

import de.weatherapp.data.entity.WeatherForecast;

/**
 * Created by emanuele on 04.07.16.
 */
public interface Forecast extends ViewModel {

    double getMinTemp();

    double getMaxTemp();

    long getTimeStamp();

    String getCityName();

    String getIcon();

    String getDescription();

    List<WeatherForecast> getHourlyForecast();
}
