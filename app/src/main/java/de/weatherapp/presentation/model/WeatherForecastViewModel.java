package de.weatherapp.presentation.model;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import de.weatherapp.data.entity.City;
import de.weatherapp.data.entity.WeatherForecast;

/**
 * Created by emanuele on 07.07.16.
 * <p/>
 * This class represents the details for a whole day
 */
public class WeatherForecastViewModel implements Forecast {
    private List<WeatherForecast> mData = new ArrayList<>();
    private WeatherForecast mMinWeatherForecast;
    private WeatherForecast mMaxWeatherForecast;
    private City mCity;


    public void add(WeatherForecast w) {
        mData.add(w);
        checkMinAndMax(w);
    }

    private void checkMinAndMax(WeatherForecast w) {
        if (mMinWeatherForecast == null && mMaxWeatherForecast == null) {
            mMinWeatherForecast = mMaxWeatherForecast = w;
            return;
        }
        if (Double.compare(w.getMainWeatherInfo().getTempMin(), mMinWeatherForecast.getMainWeatherInfo().getTempMin()) < 0) {
            mMinWeatherForecast = w;
        }
        if (Double.compare(w.getMainWeatherInfo().getTempMax(),
                mMaxWeatherForecast.getMainWeatherInfo().getTempMax()) > 0) {
            mMaxWeatherForecast = w;
        }
    }

    @Override
    public double getMinTemp() {
        if (mMinWeatherForecast == null || mMinWeatherForecast.getMainWeatherInfo() == null) {
            return 0;
        }
        return mMinWeatherForecast.getMainWeatherInfo().getTempMin();
    }

    @Override
    public double getMaxTemp() {
        if (mMaxWeatherForecast == null || mMaxWeatherForecast.getMainWeatherInfo() == null) {
            return 0;
        }
        return mMaxWeatherForecast.getMainWeatherInfo().getTempMax();
    }

    @Override
    public long getTimeStamp() {
        if (mData.isEmpty()) {
            return 0;
        }
        return mData.get(0).getDt() * 1000;
    }


    @Override
    public String getCityName() {
        if (mCity == null) {
            return null;
        }
        return mCity.getName();
    }

    public String getDescription() {
        if (mData == null || mData.isEmpty()) {
            return null;
        }
        return mData.get(0).getDescription();
    }

    public String getIcon() {
        if (mData == null || mData.isEmpty()) {
            return null;
        }
        return  mData.get(0).getIcon();
    }

    @Override
    public List<WeatherForecast> getHourlyForecast() {
        return mData;
    }

    public void setCity(City city) {
        mCity = city;
    }
}
