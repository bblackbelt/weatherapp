package de.weatherapp.presentation.presenter;

/**
 * Created by emanuele on 13.07.16.
 */
public interface PresenterFactory<T extends Presenter> {
    T createPresenter();
}
