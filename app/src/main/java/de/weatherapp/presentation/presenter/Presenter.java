package de.weatherapp.presentation.presenter;

/**
 * Created by emanuele on 03.07.16.
 */
public interface Presenter {
    void resume();

    void pause();

    void destroy();

    void unsubscribe();

    void onStart();
}
