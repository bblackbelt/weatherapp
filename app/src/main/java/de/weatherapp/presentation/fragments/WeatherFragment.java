package de.weatherapp.presentation.fragments;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.weatherapp.R;
import de.weatherapp.presentation.Utils;
import de.weatherapp.presentation.adapter.FiveDaysForecastAdapter;
import de.weatherapp.presentation.model.LocationWeatherViewModel;
import de.weatherapp.presentation.model.WeatherForecastViewModel;

/**
 * Created by emanuele on 07.07.16.
 */
public class WeatherFragment extends Fragment {

    private LocationWeatherViewModel mWeatherData;
    private WeatherForecastViewModel mToday;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.weather_fragment_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setWeatherData(mWeatherData);
    }


    public void setWeatherData(LocationWeatherViewModel weatherData) {
        if (weatherData == null || weatherData.getDayWeatherInfo().isEmpty()) {
            return;
        }
        mWeatherData = weatherData;
        Map<String, WeatherForecastViewModel> data = new LinkedHashMap<>(mWeatherData.getDayWeatherInfo());
        mToday = data.remove(Utils.getToday());
        List<WeatherForecastViewModel> nextDays = new ArrayList<>(data.values());
        if (mToday == null) {
            mToday = nextDays.remove(0);
        }
        updateUi(nextDays);
    }

    private void updateUi(List<WeatherForecastViewModel> data) {
        final View view;
        if ((view = getView()) == null) {
            return;
        }
        if (mToday != null) {
            final String tempString = Utils.kelvinToCelsius(mToday.getMinTemp()).concat(" / ").concat(Utils.kelvinToCelsius(mToday.getMaxTemp()));
            Spannable span = new SpannableString(tempString);
            final String minTempString = Utils.kelvinToCelsius(mToday.getMinTemp());
            int start = tempString.indexOf(minTempString);
            span.setSpan(new RelativeSizeSpan(0.7f), start, minTempString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            ((TextView) view.findViewById(R.id.temp)).setText(span);
            ((TextView) view.findViewById(R.id.date)).setText(Utils.getBigFormatDate(mToday.getTimeStamp()));
            ((TextView) view.findViewById(R.id.weather_description)).setText(mToday.getDescription());
            if (!TextUtils.isEmpty(mToday.getIcon())) {
                Picasso.with(getContext()).load("http://openweathermap.org/img/w/"
                        .concat(mToday.getIcon())).into((ImageView) view.findViewById(R.id.weather_icon));
            }
        }
        RecyclerView recyclerView = (RecyclerView) getView().findViewById(R.id.next_days);
        int orientation = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE
                ? LinearLayoutManager.VERTICAL
                : LinearLayoutManager.HORIZONTAL;
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), orientation, false));
        FiveDaysForecastAdapter adapter = new FiveDaysForecastAdapter();
        adapter.setData(data);
        recyclerView.setAdapter(adapter);
    }
}
