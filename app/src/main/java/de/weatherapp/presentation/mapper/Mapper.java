package de.weatherapp.presentation.mapper;

import java.util.Collection;

import de.weatherapp.data.entity.Entity;
import de.weatherapp.presentation.model.ViewModel;

/**
 * Created by emanuele on 04.07.16.
 */
public interface Mapper<T extends Entity, E extends ViewModel> {

    E transform(T entity);

    Collection<E> transform(Collection<T> entities);

}